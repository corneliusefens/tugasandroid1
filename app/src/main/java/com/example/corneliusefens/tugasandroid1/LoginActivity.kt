package com.example.corneliusefens.tugasandroid1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.corneliusefens.tugasandroid1.model.User

class LoginActivity : AppCompatActivity(), FormLoginFragment.FormLoginActionListener {
    private lateinit var loginStatusFragment: LoginStatusFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loadFragmentForm()
        loadFragmentResult()
    }

    private fun loadFragmentResult() {
        this.loginStatusFragment = LoginStatusFragment.newInstance()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.login_result_fragment, loginStatusFragment)
            commit()
        }
    }

    private fun loadFragmentForm() {
        val formLoginFragment = FormLoginFragment.newInstance(this)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.form_login_fragment, formLoginFragment)
            commit()
        }
    }

    override fun onFormLoginReady(user: User) {
        loginStatusFragment.receiveUser(user)
    }
}
